import { Component, OnInit } from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-convertisseur-devise',
  templateUrl: './convertisseur-devise.component.html',
  styleUrls: ['./convertisseur-devise.component.scss']
})
export class ConvertisseurDeviseComponent implements OnInit {
//initialiser le jeux de donnée qui vas etre utilisé
  APIkey:string="b4ad9de0-8400-11ec-aa4f-35b6a46dc411";
  from:string="EUR";
  optionFromCurrency!:string;
  optionTocurrency!:string;
  typedAmount!:number;
  result!:number;
  post!:any;
  convert!:any;

  constructor(private http:HttpClient) {}

  ngOnInit(): void {
    this.getData(this.APIkey,this.from);
  }
//Http client prend les datas
  getData(apiKey:string, from:string){
    this.http.get(this.apiCall(apiKey,from))
      .subscribe(response => {
        this.post=response;
        this.convert=this.post;
        this.post=Object.keys(this.post.data);
      })
  }

  // configurer l'url de l'api apikey est la clé de l'utilisateur et la devise
  apiCall(APIkey:string,from:string){
    return `https://freecurrencyapi.net/api/v2/latest?apikey=
    ${APIkey}&base_currency=${from}`
  }

  getCurrencyFrom(event:any){
    this.optionFromCurrency=event.target.value;
    if(this.optionFromCurrency !="EUR"){
      this.getData(this.APIkey,this.optionFromCurrency);
    }
    console.log(this.optionFromCurrency);
  }

  getCurrencyTo(event:any){
   this.optionTocurrency=event.target.value;
   console.log(this.optionTocurrency);
  }

// il faut choisir les deux devises et le montant pour activer le bouton
  statusButton(){
    return !(this.optionFromCurrency && this.typedAmount && this.optionTocurrency)
  }
//action du bouton obtenir et calculer la valeur
  showText() {
    if(this.optionFromCurrency=="EUR"){
      this.result=(Math.round(this.convert.data[this.optionTocurrency]*
        this.typedAmount*1000)/1000);
    }else if(this.optionFromCurrency==this.optionTocurrency){
      this.result=this.typedAmount;
    } else{
      this.result=(Math.round(this.convert.data[this.optionTocurrency]*
        this.typedAmount*1000)/1000);
    }
  }
}


